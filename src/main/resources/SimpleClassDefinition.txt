#ifndef nameOfClass_H
#define nameOfClass_H

#include "commonUtils.h"
#include "Utils.h"

class nameOfClass {

private:

public:

	nameOfClass() {

	}
	

	virtual std::string toString() {
		return this->toJson();
	}

	virtual std::string toJson() {
        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        /*
        JsonUtil::addMemberToDocument_From_String_String_Pair(doc, "id", id);
        JsonUtil::addMemberToDocument_From_String_Long_Pair(doc, "dateModified", dateModified);
        JsonUtil::addMemberToDocument_From_String_Boolean_Pair(doc, "IsSecure", IsSecure);
        */
        return JsonUtil::docToString(doc);
	}

	virtual ~nameOfClass() {

	}

};
typedef std::shared_ptr<nameOfClass> nameOfClassPtr;
typedef std::vector<nameOfClassPtr> nameOfClassPtrList;

#endif
