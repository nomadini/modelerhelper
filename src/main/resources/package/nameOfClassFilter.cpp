
#include "Status.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "TargetGroupFilterStatsHelper.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "nameOfClassFilter.h"

	nameOfClassFilter::nameOfClassFilter() {

	}

	std::string nameOfClassFilter::toString() {
		std::string str;

		return str;

	}

	 std::string nameOfClassFilter::getName() {
		return StringUtil::toStr("nameOfClassFilter");
	}

	std::string nameOfClassFilter::toJson() {
		std::string json;

		return json;

	}
	TargetGroupPtrVector nameOfClassFilter::filterTargetGroups(
			OpportunityContextPtr context) {
		TargetGroupPtrVector tgs = context->targetGroups;
//		TargetGroupFilterStatsHelper::logTheFilterNumbers(TargetGroupFilterStatsHelper::nameOfClassFilterName, "started", tgs);
		TargetGroupPtrVector allTgsFilteredIn;
//		for (TargetGroupPtrVector::iterator it = tgs.begin(); it != tgs.end();
//				++it) {
//			TargetGroupPtr tg = *it;
//			auto tgGeoSegmentListPtr =
//					TargetGroupCacheService::allTgGeoSegmentsListMap.find(tg->id);
//			if (tgGeoSegmentListPtr
//					!= TargetGroupCacheService::allTgGeoSegmentsListMap.end()) {
//				std::vector<GeoSegmentPtr> geoSegmentListForTg =
//						tgGeoSegmentListPtr->second;
//				for (std::vector<GeoSegmentPtr>::iterator it =
//						geoSegmentListForTg.begin();
//						it != geoSegmentListForTg.end(); ++it) {
//
//					if (context->visitorLat == 0 || context->visitorLon == 0) {
//						traceme("this user doesnt have lat lon  info needed");
//						allTgsFilteredIn.push_back(tg);
//					} else {
//						double distanceWithGeoSegment =
//								gicapods::Util::getLatLonDistanceInMile(
//										context->visitorLat,
//										context->visitorLon, (*it)->lat,
//										(*it)->lon);
//						if (distanceWithGeoSegment
//								< (*it)->segmentRadiusInMile) {
//							//this user is in the segment right now.
//							//add him to the passing filters;
//							allTgsFilteredIn.push_back(tg);
//						} else {
//							TargetGroupFilterStatsHelper::logWhyFiltered(TargetGroupFilterStatsHelper::geoSegmentFilterName, tg,
//									StringUtil::toStr(context->visitorLat) + StringUtil::toStr(":")
//											+ StringUtil::toStr(context->visitorLon),
//									StringUtil::toStr((*it)->lat) + StringUtil::toStr(":")
//											+ StringUtil::toStr((*it)->lon));
//							TargetGroupFilterStatsHelper::recordFilterTargetGroupStatistics(
//									TargetGroupFilterStatsHelper::geoSegmentFilterName, tg->id);
//							tg->countFilter(TargetGroupFilterStatsHelper::geoSegmentFilterName);
//						}
//					}
//				}
//			} else {
//				allTgsFilteredIn.push_back(tg);
//			}
//		}
//		TargetGroupFilterStatsHelper::logTheFilterNumbers(TargetGroupFilterStatsHelper::nameOfClassFilterName, "ended",
//				allTgsFilteredIn);

//		return allTgsFilteredIn;
	}

	 gicapods::StatusPtr nameOfClassFilter::process(OpportunityContextPtr context) {

		traceme("context content : {} ", context->toString());
		context->filteredTargetGroups = filterTargetGroups(context);
		if (context->filteredTargetGroups.empty()) {
			context->status->value = NO_BID_RESPONSE;
		} else {
			context->status->value = CONTINUE_PROCESSING;
		}

		return context->status;
	}
	 nameOfClassFilter::~nameOfClassFilter() {

		//delete logger;
	}