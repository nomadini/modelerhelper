#ifndef nameOfClassRequestHandler_H
#define nameOfClassRequestHandler_H



#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"

#include <string>
#include <memory>
#include <vector>
#include <set>

class nameOfClassRequestHandler;
typedef std::shared_ptr <nameOfClassRequestHandler> nameOfClassRequestHandlerPtr;

class nameOfClassRequestHandler : public Poco::Net::HTTPRequestHandler {

public:

    void handleRequest(Poco::Net::HTTPServerRequest &request ,
                       Poco::Net::HTTPServerResponse &response);

};
#endif
