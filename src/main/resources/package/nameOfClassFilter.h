/*
 * nameOfClassFilter.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mtaabodi
 */

#ifndef nameOfClass_H_
#define nameOfClass_H_



#include "Status.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "nameOfClass.h"
#include "OpportunityContext.h"
#include "TargetGroupFilterStatsHelper.h"
#include <memory>
#include <string>
#include "BidderModule.h"

class nameOfClassFilter: public BidderModule{

private:

public:

	nameOfClassFilter();

	std::string toString();

	virtual std::string getName();

	std::string toJson() ;

	TargetGroupPtrVector filterTargetGroups(OpportunityContextPtr context);

	virtual gicapods::StatusPtr process(OpportunityContextPtr context);
	virtual ~nameOfClassFilter();
};

typedef std::shared_ptr<nameOfClassFilter> nameOfClassFilterPtr;

#endif /* nameOfClass_H_ */
