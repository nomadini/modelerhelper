

#include "nameOfClass.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "nameOfClassCacheService.h"
#include "MySqlnameOfClassService.h"

std::vector<nameOfClassPtr> nameOfClassCacheService::allnameOfClasss;
std::unordered_map<int , nameOfClassPtr> nameOfClassCacheService::allnameOfClasssMap;

nameOfClassCacheServicePtr nameOfClassCacheService::getService() {
        static nameOfClassCacheServicePtr srv = std::make_shared<nameOfClassCacheService>();
        return srv;
}
nameOfClassCacheService::nameOfClassCacheService(){
    reloadCaches();
}
void nameOfClassCacheService::clearCaches() {
	allnameOfClasss.clear();
	allnameOfClasssMap.clear();
}

void nameOfClassCacheService::reloadCaches() {
		allnameOfClasss =
		MySqlnameOfClassService::getService()->readAllnameOfClasss();
		infome("{} nameOfClasss were loaded....", allnameOfClasss.size());

		allnameOfClasssMap =
		CollectionUtil::convertListToMap<nameOfClassPtr>(
				allnameOfClasss);
		infome("size of allnameOfClasssMap is {} ", allnameOfClasssMap.size());
		CollectionUtil::printTheMap<nameOfClassPtr>(nameOfClassCacheService::allnameOfClasssMap);

		}
