//
// Created by Mahmoud Taabodi on 11/19/15.
//

#ifndef nameOfClass_H
#define nameOfClass_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <set>

class nameOfClassDto;

typedef std::shared_ptr <nameOfClassDto> nameOfClassDtoPtr;

/**
 * nameOfClassDto is the class that all models extend it.
 */

class nameOfClassDto : public std::enable_shared_from_this<nameOfClassDto>{

private:

public:

//    std::string name;
//    std::string processResultStr;

    virtual void validate() ;

    nameOfClassDto() ;

    virtual std::string toString() ;

    virtual std::string toJson() ;

    static nameOfClassDtoPtr fromJson(std::string json);

    virtual ~nameOfClassDto() ;

};

#endif
