
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "nameOfClassRequestHandlerFactory.h"
#include "Utils.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

    		nameOfClassRequestHandlerFactory::nameOfClassRequestHandlerFactory() {

    		}

    		HTTPRequestHandler* nameOfClassRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request) {

    			traceme(
    					"nameOfClassRequestHandlerFactory : request uri to process : {}",
    					request.getURI());

    			if (StringUtil::contains(request.getURI(), "/nameOfClass")) {
    				//TODO : add a counter here
    				return new nameOfClassRequestHandler();
    			} else {
                    //TODO : add a counter here
    				return new UnknownRequestHandler();
    			}
    		}
