
#include "Utils.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlnameOfClassService.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

MySqlnameOfClassServicePtr MySqlnameOfClassService::getService() {
    static MySqlnameOfClassServicePtr srv = std::make_shared<MySqlnameOfClassService>(MySqlDriver::getMySqlDriver());
    return srv;
}

MySqlnameOfClassService::MySqlnameOfClassService(MySqlDriverPtr driver) {
        this->driver = driver;

}

MySqlnameOfClassService::~MySqlnameOfClassService() {

}

    std::vector<nameOfClassPtr> MySqlnameOfClassService::readAll() {
//		std::vector<nameOfClassPtr> all;
//		try {
//			traceme("readAllInventories");
//
//			sql::Statement *stmt;
//			sql::ResultSet *res;
//
//			stmt = this->driver->con->createStatement();
//
//			res = stmt->executeQuery("SELECT "
//			        "`ID`,"
//					" `NAME`,"
//					" `CATEGORY`,"
//					" `TYPE`,"
//					" `DAILY_LIMIT`,"
//					" `STATUS`"
//					" FROM `gicapods`.`inventory`;");
//
//			while (res->next()) {
//				traceme("nameOfClass loaded : id {} ", res->getString(1)); // getInt(1) returns the first column
//				nameOfClassPtr inv(new nameOfClass());
//
//				inv->id = res->getInt(1);
//
//				inv->name = res->getString(2);
//				inv->category = res->getString(3);
//				inv->type = res->getString(4);
//				inv->dailyLimit = res->getInt(5);
//				inv->status = res->getString(6);
//				all.push_back(inv);
//			}
//		} catch (sql::SQLException &e) {
//			driver->logTheError(e);
//		}
//		return all;
    }

    nameOfClassPtr MySqlnameOfClassService::read(int id) {
//        nameOfClassPtr obj(new nameOfClass());
//            try {
//
//                traceme("readnameOfClass");
//
//                sql::Statement * stmt;
//                sql::ResultSet * res;
//
//
//                std::string query = "SELECT `ID`," //1
//                        " `NAME`," //2
//                        " `ADVERTISER_ID`," //3
//                        " `LEVEL_ID`," //5
//                        " `DATE_CREATED`," //6
//                        " `DATE_MODIFIED`" //7
//                        " FROM `gicapods`.`nameOfClass` where NAME = '__NAME__'";
//
//
//                query = StringUtil::replaceString(query, "__NAME__", obj->name);
//                traceme("query : {}", query);
//
//                stmt = driver->con -> createStatement();
//                res = stmt -> executeQuery(query);
//
//
//                while (res -> next()) {
//                  //nameOfClassPtr nameOfClass (new nameOfClass());
////                nameOfClass->id = res -> getInt(1);
////                nameOfClass->Name = res -> getString(2);
//                }
//
//                delete res;
//                delete stmt;
//            } catch (sql::SQLException & e) {
//                driver->logTheError(e);
//            }
//            return nameOfClass;
    }

    void MySqlnameOfClassService::update(nameOfClassPtr obj) {

    }

    void MySqlnameOfClassService::insert(nameOfClassPtr obj) {

		traceme("inserting new nameOfClass in db : {}", obj->toJson());
//      try {
//			std::string queryStr =
//				"INSERT INTO nameOfClass"
//                " ("
//                " NAME,"
//                " ADVERTISER_ID,"
//                " LEVEL_ID,"
//                " DATE_CREATED,"
//                " DATE_MODIFIED)  "
//                " VALUES"
//                " ("
//                " '__NAME__',"
//                " '__ADVERTISER_ID__',"
//                " '__LEVEL_ID__',"
//                " '__DATE_CREATED__',"
//                " '__DATE_MODIFIED__'"
//                " );";
//
//            Timestamp now;
//            std::string date = DateTimeUtil::getNowInStringFormat(now, "%Y-%m-%d %H:%M:%s");
//            traceme("date to insert in mysql is : {}", date);
//
//			queryStr = StringUtil::replaceString(queryStr, "__NAME__",
//			obj->name);
//			queryStr = StringUtil::replaceString(queryStr, "__ADVERTISER_ID__",
//			StringUtil::toStr(obj -> advertiserId));
//			queryStr = StringUtil::replaceString(queryStr, "__LEVEL_ID__", StringUtil::toStr(obj -> levelId));
//
//            queryStr = StringUtil::replaceString(queryStr, "__DATE_CREATED__", date);
//
//            queryStr = StringUtil::replaceString(queryStr, "__DATE_MODIFIED__", date);
//
//
//
//			traceme("queryStr : {}", queryStr);
//			driver -> executedUpdateStatement(queryStr);
//           obj->id = driver->getLastInsertedId();
//		} catch (sql::SQLException & e) {
//			 driver->logTheError(e);
//		}
//
    }

    void MySqlnameOfClassService::deleteAll() {
    		try {
    			traceme("deleteAll");

    			std::string queryStr = " DELETE from nameOfClass ";
    			traceme("queryStr : {}", queryStr);
    			driver -> executedUpdateStatement(queryStr);
    		} catch (sql::SQLException & e) {
    			 driver->logTheError(e);
    		}
    }
