#ifndef nameOfClass_H
#define nameOfClass_H


#include "Utils.h"
#include <memory>
#include <string>
#include <vector>
#include <set>

class nameOfClass;
typedef std::shared_ptr<nameOfClass> nameOfClassPtr;
class nameOfClass {

public:
    nameOfClass();

    std::string toJson();
    std::string toString();
    virtual ~nameOfClass();
};

#endif