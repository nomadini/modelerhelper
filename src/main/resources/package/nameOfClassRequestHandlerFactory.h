#ifndef nameOfClassRequestHandlerFactory_H
#define nameOfClassRequestHandlerFactory_H

#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"

#include "Utils.h"
#include "UnknownRequestHandler.h"
#include "nameOfClassRequestHandler.h"


using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServerRequest;

class nameOfClassRequestHandlerFactory: public Poco::Net::HTTPRequestHandlerFactory {

	public:

		nameOfClassRequestHandlerFactory();

		HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);
	};

#endif