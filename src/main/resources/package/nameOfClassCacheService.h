#ifndef nameOfClassCacheService_h
#define nameOfClassCacheService_h


#include "nameOfClass.h"
#include <string>
#include <memory>
#include <unordered_map>
#include "CollectionUtil.h"



class nameOfClassCacheService;
typedef std::shared_ptr<nameOfClassCacheService> nameOfClassCacheServicePtr;

class nameOfClassCacheService {

public:

    static  std::vector<nameOfClassPtr> allnameOfClasss;
    static std::unordered_map <int , nameOfClassPtr> allnameOfClasssMap;

    static nameOfClassCacheServicePtr getService();


	nameOfClassCacheService();

    static void clearCaches();
    static void reloadCaches();

};


#endif
