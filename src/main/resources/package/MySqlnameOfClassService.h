#ifndef MySqlnameOfClassService_h
#define MySqlnameOfClassService_h

#include "Utils.h"

#include "CollectionUtil.h"
#include "MySqlDriver.h"
#include "nameOfClass.h"

class MySqlnameOfClassService;

typedef std::shared_ptr<MySqlnameOfClassService> MySqlnameOfClassServicePtr;
typedef std::vector<MySqlnameOfClassServicePtr> MySqlnameOfClassServicePtrList;

class MySqlnameOfClassService {

public:

    static MySqlnameOfClassServicePtr getService();

	MySqlDriverPtr driver;

    MySqlnameOfClassService(MySqlDriverPtr driver);

    std::vector<nameOfClassPtr> readAll();
    nameOfClassPtr read(int id);
    void update(nameOfClassPtr obj);
    void insert(nameOfClassPtr obj);
    virtual void deleteAll();
    virtual ~MySqlnameOfClassService();

};


#endif
