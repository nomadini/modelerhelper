

#include "nameOfClassTests.h"
#include "Utils.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "TargetGroupTestHelper.h"
#include "CampaignTestHelper.h"
#include "CreativeTestHelper.h"
#include "BidRequestTestHelper.h"
#include "OpenRtbBidRequest2_3.h"
#include "OpenRtbBidResponse2_3.h"
#include "CacheManagerTestHelper.h"
#include "OpportunityContext.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BidderTester.h"
#include "ApplicationContext.h"
#include "MySqlCampaignService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCreativeService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "BidderFunctionalTestsHelper.h"
#include "OpportunityContextTestUtil.h"
#include "BidderResponseTestUtil.h"


/*

    nameOfClassTests::nameOfClassTests() {
    }

     virtual nameOfClassTests::~nameOfClassTests() {

     }

     void nameOfClassTests::SetUp() {

     }

     void nameOfClassTests::TearDown() {

     }
*/
//TEST_F(nameOfClassTests, sendABidRequestAndTestPacingByDollar) {
//    traceme("starting sendABidRequestGetNoBid test");
//
//
//    /*
//        preparing the data in mysql
//    */
//	nameOfClassTests::setupTargetGroupAndCreativeForBidRequest();
//	nameOfClassTests::setupGeoSegments();
//    /*
//        preparing the bid request and fixing the data to target it
//    */
//    nameOfClassTests::givenABidRequest();
//	PacingPlanPtr pacingPlan(new PacingPlan());
//    pacingPlan->set(10, DateTimeUtil::getHourNowAsIntegerInUTC());
//
//	nameOfClassTests::givenTargetGroupWithUnMetPacingPlan(pacingPlan);
//
//
//    nameOfClassTests::persistTheDataInMysql();
//	/*
//	* we need to make sure that we get the proper bid
//	*/
//	OpenRtbBidResponse2_3Ptr bidResponse(new OpenRtbBidResponse2_3());
//	nameOfClassTests::whenRequestIsSentToBidder();
//	nameOfClassTests::thenBidderResponseIs(bidResponse);
//
//	OpportunityContextPtr expectedContext(new OpportunityContext());
//    nameOfClassTests::whenContextIsAskedFor();
//    nameOfClassTests::thenContextIs(expectedContext);
//
//	/*
//	calling the adserver , playing the role of exchange
//	*/
//    BidderFunctionalTestsHelper::hitAdserver(bidResponse->toJson());
//    nameOfClassTests::whenContextIsAskedForFromAdserver();
//
//    OpportunityContextPtr expectedAdserverContext(new OpportunityContext());
//    nameOfClassTests::whenAdserverContextIs(expectedAdserverContext);
//
//    /*
//       asking the pacer to force flush and merge all the changes with cassandra
//    */
//    nameOfClassTests::whenRequestForForceFlushIsSentToPacing();
//
//    /*
//        calling the pacer to give us the real time infos,
//    */
//	nameOfClassTests::whenTgRealTimeInfoIsAskedFromPacing();
//	TargetGroupRealTimeInfoPtr tgExpected = std::make_shared<TargetGroupRealTimeInfo>();
//	nameOfClassTests::thenTgRealTimeInfoIsAsExpected(tgExpected);
//
//	nameOfClassTests::whenCampaignRealTimeInfoIsAskedFromPacing();
//
//	CampaignRealTimeInfoPtr  expectedCmpRealTimeInfo = std::make_shared<CampaignRealTimeInfo>();
//	nameOfClassTests::thenCampaignRealTimeInfoIsAsExpected(expectedCmpRealTimeInfo);
//
//
//    /*
//        reading the real time info from cassandra to make sure that the changes are persisted
//        correctly
//    */
//	nameOfClassTests::whenTgRealTimeInfoIsReadFromCassandra();
//
//	nameOfClassTests::thenTgRealTimeInfoIsAsExpected(tgExpected);
//
//	nameOfClassTests::whenCampaignRealTimeInfoIsReadFromCassandra();
//
//	nameOfClassTests::thenCampaignRealTimeInfoIsAsExpected(expectedCmpRealTimeInfo);
//
//    traceme("end of sendABidRequestGetNoBid test!");
//}

