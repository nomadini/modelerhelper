

#include "signalHandler.h"
#include "ApplicationContext.h"
#include "nameOfClassPersistenceService.h"
#include "StringUtil.h"
#include "nameOfClassRequestHandler.h"
#include "Utils.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "CounterService.h"
#include "LogUtil.h"

    nameOfClassRequestHandler::nameOfClassRequestHandler() {

    }

    void nameOfClassRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request ,
                                                  Poco::Net::HTTPServerResponse &response) {

            try {

                Poco::Timestamp now;

                traceme ("Request from {} " , request.clientAddress ().toString ());


            } catch (const std::exception &e) {
                errorme ("error happening when handling request {} " ,
                         boost::diagnostic_information (e));
                infome ("sending no bad request as a result of exception");
                CounterService::getCounterService()->count (
                        "numberOfExceptionsInHandlingRequest");
                HttpUtil::setBadRequestResponse (response, e.what());
            }

            catch (...) {
                errorme ("unknown error happening when handling request");
                infome ("sending no bad request as a result of exception");
                CounterService::getCounterService()->count (
                        "numberOfExceptionsInHandlingRequest");
                HttpUtil::setBadRequestResponse (response, "unknown error");
            }

            LogUtil::flushTheLogs ();
        }

