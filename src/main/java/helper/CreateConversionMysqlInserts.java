package helper;

import com.mysql.jdbc.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CreateConversionMysqlInserts {

    private static final String COMMA_SPACE = ", ";
    static ConversionStreamWriter writer = new ConversionStreamWriter();
    static Random r = new Random();

    public static void main(String[] args) throws InterruptedException {
        // System.out.println(hashIpAddressForLogs("74.88.72.106, 74.88.72.106, 184.169.192.185"));
        // String ipAddr = "74.88.72.106, 74.88.72.106, 184.169.192.185";
        // String ipAddr = "107.188.26.46, 107.188.26.46, 54.219.212.98";
        // if (ipAddr.contains(COMMA_SPACE)) {
        // String clientIpAddress = ipAddr.split(COMMA_SPACE)[0];
        // System.out.println(clientIpAddress);
        // }
//         createRandomConversions();
        CreateConversionMysqlInserts inserts = new CreateConversionMysqlInserts();
        inserts.createRandomImpression();
        while (true) {
//            insertRandomImpressions();
//            Thread.sleep(1000);
        }
    }

    private void insertRandomImpressions() {
        try {
            List<String> queries = createRandomImpression();
            for (String q : queries) {
                    writer.writeConversions(q);

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    class DataSet {

        int targetGroupId;
        int campaignId;
        int advertiserId;
        int clientId;
        int geossegment[];
        int creativeIds [];
    }
    private List<String> createRandomImpression() {

        List<String> allSqls = new ArrayList<>();
        int numberOfInt = 100 + Math.abs(r.nextInt() % 10);



       DataSet myDataSet2 = new DataSet ();

//       SET A
//        myDataSet2.targetGroupId = 23;
//        myDataSet2.campaignId = 15;
//        myDataSet2.advertiserId = 4;
//        myDataSet2.clientId = 4;
//        myDataSet2.geossegment = new int[]{ 7,8,12,13,18};
//        myDataSet2.creativeIds= new int[]{5, 24, 25};


        //SET B
        myDataSet2.targetGroupId = 22;
        myDataSet2.campaignId = 16;
        myDataSet2.advertiserId = 8;
        myDataSet2.clientId = 3;
        myDataSet2.geossegment = new int[]{9};
        myDataSet2.creativeIds= new int[]{2,9, 10, 21};



        ArrayList<DataSet> allDataSets = new ArrayList<>();
//        allDataSets.add(myDataSet1);
        allDataSets.add(myDataSet2);

        ArrayList<String> eventTypes = new ArrayList<>();
        eventTypes.add("impression");
        eventTypes.add("click");
        eventTypes.add("conversion");

        //NJ
        Double latitude = 40.787743d;
        Double longitude = -74.067649;

        //PA
        latitude = 40.787743;
        longitude =  -74.067649;
        Random rand = new Random();
    for(DataSet myDataSet : allDataSets) {
        for(String eventType :eventTypes) {
            for (int numberOfImp = 0; numberOfImp < numberOfInt; numberOfImp++) {
                int geoSegmentId = myDataSet.geossegment[Math.abs(r.nextInt() % myDataSet.geossegment.length)];
                int creativeId = myDataSet.creativeIds[Math.abs(r.nextInt() % myDataSet.creativeIds.length)];
                int hoursDifference = Math.abs(r.nextInt() % 10);

                int recordId = Math.abs(r.nextInt() * r.nextInt() * r.nextInt() * r.nextInt() * r.nextInt());
                String sql = "INSERT INTO `new_mango`.`impression` (`id`, `event_type`,`targetgroup_id`," +
                        " creative_id, campaign_id, advertiser_id, client_id, geosegment_id, latitude, longitude, updated_at, created_at)"
                        + " VALUES" + "(__RECORD__ID__, '__EVENT_TYPE__', __TARGET_GROUP_ID__, " +
                        " __CREATIVE_ID__, __CAMPAIGN_ID__, __ADVERTISER_ID__, __CLIENT_ID__, __GEO_SEGMENT_ID__, __LAT__, __LON__ , " +
                        " DATE_SUB( NOW( ) , INTERVAL __HOUR_DIFF__ MINUTE ), DATE_SUB( NOW( ) , INTERVAL __HOUR_DIFF__ MINUTE ));";

                sql = sql.replaceAll("__RECORD__ID__", String.valueOf(recordId));
                sql = sql.replaceAll("__EVENT_TYPE__", String.valueOf(eventType));
                sql = sql.replaceAll("__TARGET_GROUP_ID__", String.valueOf(myDataSet.targetGroupId));
                sql = sql.replaceAll("__CREATIVE_ID__", String.valueOf(creativeId));
                sql = sql.replaceAll("__CAMPAIGN_ID__", String.valueOf(myDataSet.campaignId));
                sql = sql.replaceAll("__ADVERTISER_ID__", String.valueOf(myDataSet.advertiserId));
                sql = sql.replaceAll("__CLIENT_ID__", String.valueOf(myDataSet.clientId));
                sql = sql.replaceAll("__GEO_SEGMENT_ID__", String.valueOf(geoSegmentId));
                sql = sql.replaceAll("__HOUR_DIFF__", String.valueOf(hoursDifference));
                sql = sql.replaceAll("__HOUR_DIFF__", String.valueOf(hoursDifference));
                sql = sql.replaceAll("__LAT__", String.valueOf(latitude + rand.nextFloat()));
                sql = sql.replaceAll("__LON__", String.valueOf(longitude + rand.nextFloat()));


                System.out.println(sql);
                allSqls.add(sql);
                // }
            }
        }
    }
        return allSqls;

    }
}
