package helper;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

public class SVMDataProvider {


	private void createModelInputs() {
		HashMap<Integer, Integer> indexAndValueMap = new HashMap<>();
		String fileContent = "";

		for (int j = 0; j < Util.numberOfUsers; j++) {
			int converted = -1;
			indexAndValueMap = new HashMap<>();
			// shuffle the list for this user
			long seed = System.nanoTime();
			Collections.shuffle(Util.historyOfUserList, new Random(seed));
			// get a random number that is at most the size of history list
			int numberOfVisitsByUser = 0;
			do {
				numberOfVisitsByUser = Util.rand.nextInt(Util.historyOfUserList.size());

				if (numberOfVisitsByUser > 0) {
					for (int i = 0; i < numberOfVisitsByUser; i++) {

						if (Util.isQualifiedBasedOnProbability(Util.historyOfUserList.get(i).hashCode())) {
							indexAndValueMap.put(Util.getIndexOf(Util.historyOfUserList.get(i)), 1);
							converted = 1;

						} else {
							indexAndValueMap.put(Util.getIndexOf(Util.historyOfUserList.get(i)), 1);
						}
					}

					String row = createTheRowContent(converted, indexAndValueMap) + "\n";
					Util.echo("row is : " + row);
					fileContent += row;

				}
			} while (numberOfVisitsByUser < 0);
		}
		Util.createTheTrainDataFile("trainDataSmallSample.txt", fileContent);
	}

	private String createTheRowContent(int converted, HashMap<Integer, Integer> featureMap) {
		String rowContent = (converted == 1 ? "+1" : "-1") + " ";
		Iterator it = featureMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			rowContent += pairs.getKey().toString() + ":" + pairs.getValue().toString() + " ";
		}
		return rowContent;
	}

	public static void main(String args[]) throws IOException {

		SVMDataProvider helper = new SVMDataProvider();
		helper.createModelInputs();
	}
}
