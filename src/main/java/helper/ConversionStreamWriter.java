package helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ConversionStreamWriter {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    static Random rand = new Random();
    static Map<String, List<String>> tgIdToCreativeIdList = new HashMap<>();
    static ConversionStreamWriter convWriter = new ConversionStreamWriter();
    class TargetGroup {
        public String tgId;
        public String cmpId;
        
        public String getTargetGroupId() {
            // TODO Auto-generated method stub
            return tgId;
        }

        public String getCampaignId() {
            // TODO Auto-generated method stub
            return cmpId;
        }
        
    }
    public static void main(String[] args) throws Exception {
        convWriter.initConnection();
        String templateStatement = "INSERT INTO `gicapods`.`impression`"+
"("+
"`TRANSACTION_ID`,"+
"`EVENT_TYPE`,"+
"`TARGETGROUP_ID`,"+
"`CREATIVE_ID`,"+
"`CAMPAIGN_ID`,"+
"`ADVERTISER_ID`,"+
"`CLIENT_ID`,"+
"`PUBLISHER_ID`,"+
"`DEVICE_TYPE_ID`,"+
"`SEGMENT_ID`,"+
"`GEO_SEGMENT_ID`,"+
"`LOCATION_ID`,"+
"`DAY_TIME_ID`,"+
"`DATE_CREATED`,"+
"`DATE_MODIFIED`)"+
"VALUES"+
"("+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"%s,"+"now(),"+"now());";
        
        List<TargetGroup> tgs = readAllTargetGroups();
        ConversionStreamWriter dao = new ConversionStreamWriter();
        while(true) {
        for (TargetGroup tg: tgs) {
            String advId = readAdvertiserIdForTgs(tg);
            String clId = readClientId(advId);
            String crId = readACreativeIdForTg(tg);
            String locationId = readALocationIdForTg(tg);
            String geoSegmentId = readGeoSegmentIdForTg(tg);
            String pubId = readPublisherId();
            int trnId = Math.abs(rand.nextInt()) * Math.abs(rand.nextInt());
            String dayTimeId  = getDayTimeIdForTg(tg);
            String segmentId = readSegmentIdForTg(tg);
            String deviceTypeId = readDeviceTypeIdForTg(tg);
            
            String s = String.format(templateStatement, trnId+"" ,getEventType(trnId),tg.getTargetGroupId(),crId,tg.getCampaignId(),advId,clId, pubId,
                    deviceTypeId,segmentId,geoSegmentId,locationId,dayTimeId);
            System.out.println("statement : "+ s);
           try{
            dao.writeConversions(s);
           }catch(Exception e){
              /// e.printStackTrace();
           }

        }
//        int val = Math.abs(rand.nextInt())%100;
//        if (val <20) {
//            Thread.sleep(0);
//            
//        }else {
            Thread.sleep(000);
//        }
        }
        // dao.readDataBase();
       // dao.writeConversions("INSERT INTO `gicapods`.`impression`(`ID`,`TRANSACTION_ID`,`TARGETGROUP_ID`,`DATE_CREATED`,`DATE_MODIFIED`)VALUES(1212,121212,12,FROM_UNIXTIME(1231763962),now());");
    }
    static Date date = new Date();
    private static String getNow() {
        return date.getTime()+"";
    }
    private static String readDeviceTypeIdForTg(TargetGroup tg) throws Exception {
        int val = Math.abs(rand.nextInt()%3);
        if (val ==0 ) return "'tablet'";
        if (val == 1 ) return "'mobile'";
        if (val == 2 ) return "'desktop'";
        throw new Exception("wrong type : val " + val);
    }
    private static String getDayTimeIdForTg(TargetGroup tg) {
        // TODO Auto-generated method stub
        return (Math.abs(rand.nextInt()%168))+"";
    }
    private static String readSegmentIdForTg(TargetGroup tg) {
        
        return  (Math.abs(rand.nextInt()%8))+"";
    }
    private static String readPublisherId() {
        return  (Math.abs(rand.nextInt()%100))+"";
    }
    private static String getEventType(int trnId) throws Exception {
        int val = Math.abs(rand.nextInt()%3);
        if ( val == 0 )return "'conversion'";
        if ( val == 1 ) return "'click'";
        if ( val == 2 ) return "'impression'";
        throw new Exception("wrong type" + val);
    }

    private static String readGeoSegmentIdForTg(TargetGroup tg) {
        // TODO Auto-generated method stub
        return (Math.abs(rand.nextInt()%10))+"";
    }
    private static String readALocationIdForTg(TargetGroup tg) {
        // TODO Auto-generated method stub
        return (Math.abs(rand.nextInt()%100))+"";
    }
    private static String readACreativeIdForTg(TargetGroup tg) throws SQLException {;
       
        if(tgIdToCreativeIdList.get(tg.getTargetGroupId())==null) {
           List<String> list = convWriter.getIdsForEntity("select CREATIVE_ID from targetgroup_creative_map where TARGET_GROUP_ID = "+tg.getTargetGroupId());
           tgIdToCreativeIdList.put(tg.getTargetGroupId(), list);
        } 
        int size = tgIdToCreativeIdList.get(tg.getTargetGroupId()).size();
        if(size ==0 ) return Math.abs(rand.nextInt())%13+"";
        return (tgIdToCreativeIdList.get(tg.getTargetGroupId()).get(Math.abs(rand.nextInt()%size)))+"";
    }
    static List<TargetGroup>  tgCache;
    private static List<TargetGroup> readAllTargetGroups() throws SQLException {
        if(tgCache==null) {
            tgCache = convWriter.getTargetGroups("select ID, CAMPAIGN_ID from targetgroup");
        }
        return tgCache;
    }
    static Map<String, String> advIdtoclId = new HashMap<>();
    private static String readClientId(String advId) throws SQLException {
        if(advIdtoclId.get(advId) ==null) {
            String clId = convWriter.getIdsForEntity("select CLIENT_ID from advertiser where ID = "+advId).get(0);
            cmpIdtoAdvId.put(advId,clId);
        }
        return cmpIdtoAdvId.get(advId);
    }
   static Map<String, String> cmpIdtoAdvId = new HashMap<>();
    private static String readAdvertiserIdForTgs(TargetGroup tg) throws SQLException {
        if(cmpIdtoAdvId.get(tg.cmpId) ==null) {
            String advId = convWriter.getIdsForEntity("select ADVERTISER_ID from campaign where ID = "+tg.cmpId).get(0);
            cmpIdtoAdvId.put(tg.cmpId, advId);
        }
        return cmpIdtoAdvId.get(tg.cmpId);
    }

    public void initConnection() throws Exception {
     // TODO Auto-generated method stub
        Class.forName("com.mysql.jdbc.Driver");
        // Setup the connection with the DB
        connect = DriverManager.getConnection("jdbc:mysql://localhost/gicapods?"
                + "user=root&password=");
        
    }
    public void writeConversions(String query) throws Exception {
        if(connect == null) {
            initConnection();
        }
        else {
            // Statements allow to issue SQL queries to the database
        statement = connect.createStatement();
        // Result set get the result of the SQL query
        statement.executeUpdate(query);
        }
    }
    public List<TargetGroup> getTargetGroups(String query) throws SQLException {
        statement = connect.createStatement();
        List<TargetGroup> results = new ArrayList<>();
        // Result set get the result of the SQL query
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);
            TargetGroup tg = new TargetGroup();
            String tgId = resultSet.getInt(1)+"";
            String cmpId = resultSet.getInt(2)+"";
            
            tg.tgId = tgId;
            tg.cmpId = cmpId;
            results.add(tg);
        }
        return results;
    }
    
    public List<String> getIdsForEntity(String query) throws SQLException {
        statement = connect.createStatement();
        List<String> results = new ArrayList<>();
        // Result set get the result of the SQL query
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);
            String id = resultSet.getString(1);
            results.add(id);
        }
        return results;
    }
    public void readDataBase() throws Exception {
        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection("jdbc:mysql://localhost/gicapods?"
                    + "user=root&password=");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement.executeQuery("select * from impression");
            writeResultSet(resultSet);

            // PreparedStatements can use variables and are more efficient
            preparedStatement = connect
                    .prepareStatement("insert into  feedback.comments values (default, ?, ?, ?, ? , ?, ?)");
            // "myuser, webpage, datum, summery, COMMENTS from feedback.comments");
            // Parameters start with 1
            preparedStatement.setString(1, "Test");
            preparedStatement.setString(2, "TestEmail");
            preparedStatement.setString(3, "TestWebpage");
            preparedStatement.setDate(4, new java.sql.Date(2009, 12, 11));
            preparedStatement.setString(5, "TestSummary");
            preparedStatement.setString(6, "TestComment");
            preparedStatement.executeUpdate();

            preparedStatement = connect
                    .prepareStatement("SELECT myuser, webpage, datum, summery, COMMENTS from feedback.comments");
            resultSet = preparedStatement.executeQuery();
            writeResultSet(resultSet);

            // Remove again the insert comment
            preparedStatement = connect
                    .prepareStatement("delete from feedback.comments where myuser= ? ; ");
            preparedStatement.setString(1, "Test");
            preparedStatement.executeUpdate();

            resultSet = statement.executeQuery("select * from feedback.comments");
            writeMetaData(resultSet);

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }

    }

    private void writeMetaData(ResultSet resultSet) throws SQLException {
        // Now get some metadata from the database
        // Result set get the result of the SQL query

        System.out.println("The columns in the table are: ");

        System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
        for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
            System.out.println("Column " + i + " " + resultSet.getMetaData().getColumnName(i));
        }
    }

    private void writeResultSet(ResultSet resultSet) throws SQLException {
        // ResultSet is initially before the first data set
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);
            String user = resultSet.getString("myuser");
            String website = resultSet.getString("webpage");
            String summery = resultSet.getString("summery");
            Date date = resultSet.getDate("datum");
            String comment = resultSet.getString("comments");
            System.out.println("User: " + user);
            System.out.println("Website: " + website);
            System.out.println("Summery: " + summery);
            System.out.println("Date: " + date);
            System.out.println("Comment: " + comment);
        }
    }

    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

}