package helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;

public class ClassHelper {

    public static void main(String args[]) throws IOException {

        ClassHelper helper = new ClassHelper();
        helper.createPackageOfClasses();
    }

    private void createPackageOfClasses() throws IOException {
        List<String> listOfClasses = new ArrayList<>();

        listOfClasses.add("MySqlnameOfClassService.cpp");
        listOfClasses.add("MySqlnameOfClassService.h");
        listOfClasses.add("nameOfClass.cpp");
        listOfClasses.add("nameOfClass.h");
        listOfClasses.add("nameOfClassRequestHandler.cpp");
        listOfClasses.add("nameOfClassRequestHandler.h");
        listOfClasses.add("nameOfClassUtil.cpp");
        listOfClasses.add("nameOfClassUtil.h");
        listOfClasses.add("nameOfClassTests.h");
        listOfClasses.add("nameOfClassTests.cpp");
        listOfClasses.add("nameOfClassTestUtil.h");
        listOfClasses.add("nameOfClassTestUtil.cpp");
        listOfClasses.add("nameOfClassDto.h");
        listOfClasses.add("nameOfClassDto.cpp");
        listOfClasses.add("nameOfClassFilter.h");
        listOfClasses.add("nameOfClassFilter.cpp");
        listOfClasses.add("nameOfClassCacheService.h");
        listOfClasses.add("nameOfClassCacheService.cpp");
        listOfClasses.add("nameOfClassRequestHandlerFactory.cpp");
        listOfClasses.add("nameOfClassRequestHandlerFactory.h");

        ClassHelper helper = new ClassHelper();
        FileUtils.cleanDirectory(new File("output"));
        for (String module : listOfClasses) {

//            helper.copyAndReplaceTheFile(module, "DeviceSegment");
            helper.copyAndReplaceTheFile(module, "SiteTestHelper");


//            helper.copyAndReplaceTheFile(module, "BWEntry");
//            helper.copyAndReplaceTheFile(module, "TargetGroupBwListMap");
        }
    }



    private void createHeader(String template,
                              String nameOfClass) {
        buildTheMainHeader(template, nameOfClass);
    }

    private void copyAndReplaceTheFile(String filename, String nameOfClass) {
        try {
            String fileContent = FileUtils.readFileToString(new File(
                    "src/main/resources/package/" + filename));
            String readyClassContent = fileContent.replaceAll("nameOfClass",
                    nameOfClass);
            String readyClassName = filename.replaceAll("nameOfClass",
                    nameOfClass);

            FileUtils.writeStringToFile(
                    new File("src/main/resources/package/output/" + readyClassName),
                    readyClassContent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void buildTheMainHeader(String template, String nameOfClass) {
        try {
            String fileContent = FileUtils.readFileToString(new File(
                    template));
            String readyClassContent = fileContent.replaceAll("nameOfClass",
                    nameOfClass);
            FileUtils.cleanDirectory(new File("output"));
            FileUtils.writeStringToFile(
                            new File("output/" + nameOfClass + ".h"),
                            readyClassContent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
