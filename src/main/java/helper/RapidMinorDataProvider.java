package helper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class RapidMinorDataProvider {

    /*
        this class creates a trainX.txt and trainY.txt
        where trainX has the features and trainY has the labels.

     */
    public static void main(String args[]) throws IOException {
        RapidMinorDataProvider helper = new RapidMinorDataProvider();
        helper.createModelInputs();
    }

    private void createModelInputs() {
        String fileContentForTrainX = "";
        String fileContentForTrainY = "";
        //get the list of features , assign an index to them
        //create a random number of history for a user
        //get a random index of history each time to get a history
        //if the probability of that history is above threshold
        //add the index to the 1index for x values
        //create a y = 1 data for it if positiveIndexs are not empty, if not create a 0
        //then put an 1 for x for any feature that is in history and
        //put a 0 for the rest

//        long seed = System.nanoTime();
//        Collections.shuffle(Util.historyOfUserList, new Random(seed));

//        Util.echo("original list : "+ Util.historyOfUserList);

        for (int numberOfData = 0; numberOfData < 5; numberOfData++) {
            for (int historyIndex = 0; historyIndex < Util.historyOfUserList.size(); historyIndex++) {


                int numberOfHistoryVisitsByUser = Util.rand.nextInt(Util.historyOfUserList.size());
                Set<Integer> positiveIndexes = new HashSet<>();
                for (int numberOfVisit = 0; numberOfVisit < numberOfHistoryVisitsByUser; numberOfVisit++) {

                    int randomIndexOfHistory = Util.rand.nextInt(Util.historyOfUserList.size());
                    if (Util.isQualifiedBasedOnProbability(Util.historyOfUserList.get(randomIndexOfHistory).hashCode())) {
                        positiveIndexes.add(randomIndexOfHistory);
                    }
                }
                String rowForXValues = createRowWithPositiveIndexesForTrainXData(positiveIndexes);
                String rowForYValues = createTheRowContentForTrainY(positiveIndexes);
                fileContentForTrainX += rowForXValues;
                fileContentForTrainX += rowForYValues + "\n";
            }
        }
        Util.createTheTrainDataFile("train-RapidMinor-Data.txt", fileContentForTrainX);

    }


    private String createRowWithPositiveIndexesForTrainXData(Set<Integer> positiveIndexes) {
//        Util.echo("positiveIndexes : " + positiveIndexes);
        String rowContent = "";
        for (int columnIndex = 0; columnIndex < Util.historyOfUserList.size(); columnIndex++) {
            if(positiveIndexes.contains(columnIndex)) {
                rowContent += "1 ";
            } else {
                rowContent += "0 ";
            }
        }
        return rowContent;
    }

    private String createTheRowContentForTrainY(Set<Integer> positiveIndexes) {
        String rowContent = (!positiveIndexes.isEmpty() ? "1" : "0") + " ";
        return rowContent;
    }
}
