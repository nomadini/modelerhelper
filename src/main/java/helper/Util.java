package helper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Util {
    static Random rand = new Random();
    public static HashMap<Integer, Integer> webSiteHashToProbabilityMap = new HashMap<>();
    public static List<String> historyOfUserList = new ArrayList<String>();
    public static HashMap<String, Integer> featureIndexMap = new HashMap<String, Integer>();

    public static String healthCom = "www.health.com";
    public static String womenDotCom = "www.women.com";
    public static String dstilleryDotCom = "www.dstillery.com";
    public static String admarketplaceDotCom = "www.admarketplace.com";
    public static String TwitterDotCom = "www.Twitter.com";
    public static String TencentDotCom = "www.Tencent.com";
    public static String GoogleDotCom = "www.Google.com";
    public static String eBayDotCom = "www.eBay.com";
    public static String BlogspotDotCom = "www.Blogspot.com";
    public static String Hao123DotCom = "www.Hao123.com";
    public static String YandexDotCom = "www.Yandex.com";
    public static String cnnDotCom = "www.cnn.com";
    public static int numberOfUsers = 1000;

    static {
        Util.echo(healthCom + ":90:" + healthCom.hashCode());
        webSiteHashToProbabilityMap.put(healthCom.hashCode(), 90);
        featureIndexMap.put(healthCom, 1);

        Util.echo(womenDotCom + ":10:" + womenDotCom.hashCode());
        webSiteHashToProbabilityMap.put(womenDotCom.hashCode(), 10);
        featureIndexMap.put(womenDotCom, 2);

        Util.echo(dstilleryDotCom + ":0:" + dstilleryDotCom.hashCode());
        webSiteHashToProbabilityMap.put(dstilleryDotCom.hashCode(), 0);
        featureIndexMap.put(dstilleryDotCom, 3);

        Util.echo(admarketplaceDotCom + ":0:" + admarketplaceDotCom.hashCode());
        webSiteHashToProbabilityMap.put(admarketplaceDotCom.hashCode(), 0);
        featureIndexMap.put(admarketplaceDotCom, 4);

        Util.echo(cnnDotCom + ":20:" + cnnDotCom.hashCode());
        webSiteHashToProbabilityMap.put(cnnDotCom.hashCode(), 20);
        featureIndexMap.put(cnnDotCom, 5);

        Util.echo(TwitterDotCom + ":00:" + TwitterDotCom.hashCode());
        webSiteHashToProbabilityMap.put(TwitterDotCom.hashCode(), 00);
        featureIndexMap.put(TwitterDotCom, 6);

        Util.echo(TencentDotCom + ":00:" + TencentDotCom.hashCode());
        webSiteHashToProbabilityMap.put(TencentDotCom.hashCode(), 00);
        featureIndexMap.put(TencentDotCom, 7);

        Util.echo(GoogleDotCom + ":00:" + GoogleDotCom.hashCode());
        webSiteHashToProbabilityMap.put(GoogleDotCom.hashCode(), 00);
        featureIndexMap.put(GoogleDotCom, 8);

        Util.echo(eBayDotCom + ":00:" + eBayDotCom.hashCode());
        webSiteHashToProbabilityMap.put(eBayDotCom.hashCode(), 00);
        featureIndexMap.put(eBayDotCom, 9);

        Util.echo(BlogspotDotCom + ":00:" + BlogspotDotCom.hashCode());
        webSiteHashToProbabilityMap.put(BlogspotDotCom.hashCode(), 00);
        featureIndexMap.put(BlogspotDotCom, 10);

        Util.echo(Hao123DotCom + ":00:" + Hao123DotCom.hashCode());
        webSiteHashToProbabilityMap.put(Hao123DotCom.hashCode(), 00);
        featureIndexMap.put(Hao123DotCom, 11);

        Util.echo(YandexDotCom + ":00:" + YandexDotCom.hashCode());
        webSiteHashToProbabilityMap.put(YandexDotCom.hashCode(), 00);
        featureIndexMap.put(YandexDotCom, 12);

        historyOfUserList.add(healthCom);
        historyOfUserList.add(womenDotCom);
        historyOfUserList.add(dstilleryDotCom);
        historyOfUserList.add(admarketplaceDotCom);
        historyOfUserList.add(cnnDotCom);

        historyOfUserList.add(TwitterDotCom);
        historyOfUserList.add(TencentDotCom);
        historyOfUserList.add(GoogleDotCom);
        historyOfUserList.add(eBayDotCom);
        historyOfUserList.add(BlogspotDotCom);
        historyOfUserList.add(Hao123DotCom);
        historyOfUserList.add(YandexDotCom);

    }

    public static void createTheTrainDataFile(String filename, String fileContent) {
        try {
            FileUtils.write(new File(filename), fileContent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void echo(String string) {
        System.out.println(string);
    }

    public static Integer getIndexOf(String nameOfFeature) {
        return Util.featureIndexMap.get(nameOfFeature);
    }


    public static boolean isQualifiedBasedOnProbability(int indexOfWebsite) {
//        Util.echo("Util.webSiteHashToProbabilityMap : " + Util.webSiteHashToProbabilityMap);
//        Util.echo("indexOfWebsite : " + indexOfWebsite + " Util.webSiteHashToProbabilityMap.get(indexOfWebsite) : " + Util.webSiteHashToProbabilityMap.get(indexOfWebsite));
        return (Util.webSiteHashToProbabilityMap.get(indexOfWebsite) > rand.nextInt(100));
    }
}
